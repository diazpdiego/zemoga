# README #

### What is this repository for? ###

The propouse of this repo is to make develop the Test. The Test consist in display a kind of list with 100 posts. Those posts have several properties and states like READ, UNREAD and FAVORITE. So, when the user tap on each post, remove the UNREAD icon in the cell and show the detail information.

The user can add to Favorites and remove from them as well. The user also can remove one post of the list or remove all. 

The main view has a Segmented Control which allow the user filter between all the posts and the favorites added. 

Version 1.0

### How do I get set up? ###

This project was developed using SplitViewController template. This project does not include any third party library. So, you can just clone the repo and run the project. 

The project has two branches. Master and develop. Now both branches has the same work. 

### Architecture ###

I decided to use a MVP pattern. This allow me to deacouple all the layers and responsabilities. So, You will find the Model Layer which should handle the object, the View layer which does not know nothign about the Model but has a strong reference to the Presenter. This is resposible to manipulate the information, and serve like a bridge between the Model and View. 

The repository Layer is who do all the request to get the POSTS, Detail information and other stuff. 

### Who do I talk to? ###

Diego Díaz
d.diaz110@hotmail.com
