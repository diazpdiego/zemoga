//
//  CommentModel.swift
//  Zemoga
//
//  Created by Diego Diaz on 25/07/20.
//  Copyright © 2020 Diego Diaz. All rights reserved.
//

import Foundation

struct CommentModel: Decodable {
    var postId: Int
    var id: Int
    var name: String?
    var email: String?
    var body: String?
}
