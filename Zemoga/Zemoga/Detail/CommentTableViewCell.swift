//
//  CommentTableViewCell.swift
//  Zemoga
//
//  Created by Diego Diaz on 25/07/20.
//  Copyright © 2020 Diego Diaz. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    //MARK: - Properties
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Configure
    
    func configure(with commentModel: CommentModel) {
        nameLabel.text = commentModel.name
        emailLabel.text = commentModel.email
        bodyLabel.text = commentModel.body
    }
    
}
