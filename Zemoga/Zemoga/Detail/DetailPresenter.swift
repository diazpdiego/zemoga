//
//  DetailPresenter.swift
//  Zemoga
//
//  Created by Diego Diaz on 25/07/20.
//  Copyright © 2020 Diego Diaz. All rights reserved.
//

import Foundation

struct DetailPresenter {
    
    //MARK: - Properties
    
    var repository = Repository()
    let decoder = JSONDecoder()

    //MARK: - Public Methods
    
    func fetchComments(to postId: Int, completion: @escaping (_ comments: [CommentModel]?) -> Void) {
        repository.getComments(to: postId, completion: { (data) in
            guard let commentsData = data else {
                completion(nil)
                return
            }
            
            let results = self.parseComments(commentsData)
            completion(results)
        })
    }
    
    func fetchUserInfo(to userId: Int, completion: @escaping (_ comments: UserModel?) -> Void) {
        repository.getUserInfo(to: userId) { (data) in
            guard let userData = data else {
                completion(nil)
                return
            }
            
            let result = self.parseUserInfo(userData)
            completion(result)
            
        }
    }
    
    //MARK: - Private Methods
    
    private func parseComments(_ data: Data) -> [CommentModel]? {
        guard let commentsModel = try? decoder.decode([CommentModel].self, from: data) else {
            return nil
        }
        
        return commentsModel
    }
    
    private func parseUserInfo(_ data: Data) -> UserModel? {
        guard let userModel = try? decoder.decode(UserModel.self, from: data) else {
            return nil
        }
        
        return userModel
    }
    
}
