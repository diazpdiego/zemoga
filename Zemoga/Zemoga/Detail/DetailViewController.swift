//
//  DetailViewController.swift
//  Zemoga
//
//  Created by Diego Diaz on 24/07/20.
//  Copyright © 2020 Diego Diaz. All rights reserved.
//

import UIKit

protocol FavoriteDelegate: class {
    func addToFavorites(_ postId: Int)
    func removeFavorite(_ postId: Int)
}

class DetailViewController: UIViewController {

    //MARK: - Config
    
    private enum Config {
        static let addFavoriteImage     = UIImage(named: "emptyStar-icon")
        static let favoriteImage        = UIImage(named: "star-icon")
        static let commentCellId        = "CommentCell"
        static let commentCellNibName   = "CommentTableViewCell"
        static let defaultCellHeight    : CGFloat = 111
    }
    
    //MARK: - Properties
    
    private var presenter = DetailPresenter()
    private var comments: [CommentModel] = []
    private var addToFavoriteButton = UIBarButtonItem()
    private var isFavorite: Bool = false
    
    weak var delegate: FavoriteDelegate?
    var postModel: PostModel? {
      didSet {
        setupUI()
      }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        guard
            let postId = postModel?.id,
            let userId = postModel?.userId else { return }
        
        fetchComments(to: postId)
        fetchUserInfo(to: userId)
    }
    
    //MARK: - Private Methods
    
    private func setupUI() {
        loadViewIfNeeded()
        
        guard let post = postModel else { return }
        isFavorite = post.isFavorite ?? false
        
        addToFavoriteButton = UIBarButtonItem(image: Config.addFavoriteImage, style: .done, target: self, action: #selector(addToFavoritesButtonTapped))
        addToFavoriteButton.image = !isFavorite ? Config.addFavoriteImage : Config.favoriteImage

        navigationItem.rightBarButtonItem = addToFavoriteButton
        
        titleLabel.text = post.title
        bodyLabel.text  = post.body
        
        let nib = UINib(nibName: Config.commentCellNibName, bundle: nil)
        commentsTableView.register(nib, forCellReuseIdentifier: Config.commentCellId)
        commentsTableView.delegate = self
        commentsTableView.dataSource = self
    }
    
    @objc
    private func addToFavoritesButtonTapped() {
        guard let postId = postModel?.id else { return }

        if !isFavorite {
            addToFavoriteButton.image = Config.favoriteImage
            delegate?.addToFavorites(postId)
            
        } else {
            addToFavoriteButton.image = Config.addFavoriteImage
            delegate?.removeFavorite(postId)
        }
        
    }
    
    private func fetchComments(to postId: Int) {
        presenter.fetchComments(to: postId) { [weak self] (commentsResult) in
            guard let commentsArray = commentsResult else { return }
            
            DispatchQueue.main.async {
                self?.comments = commentsArray
                self?.commentsTableView.reloadData()
            }
        }
    }
    
    private func fetchUserInfo(to userId: Int) {
        presenter.fetchUserInfo(to: userId) { [weak self] (userInfo) in
            guard let user = userInfo else { return }
            
            DispatchQueue.main.async {
                self?.nameLabel.text = user.name
                self?.emailLabel.text = user.email
                self?.phoneLabel.text = user.phone
                
                self?.websiteLabel.text = user.website
            }
        }
    }

}

//MARK: - Post Seleccion Delegate

extension DetailViewController: PostSelectionDelegate {
    func postSelected(_ post: PostModel) {
        postModel = post
    }
}

//MARK: - TableView Methods

extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Config.defaultCellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CommentTableViewCell = tableView.dequeueReusableCell(withIdentifier: Config.commentCellId) as! CommentTableViewCell
        cell.configure(with: comments[indexPath.row])
        
        return cell
    }
    
    
}
