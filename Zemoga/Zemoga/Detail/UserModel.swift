//
//  UserModel.swift
//  Zemoga
//
//  Created by Diego Diaz on 26/07/20.
//  Copyright © 2020 Diego Diaz. All rights reserved.
//

import Foundation

struct UserModel: Decodable {
    var id: Int
    var name: String
    var email: String
    var phone: String
    var website: String
}

