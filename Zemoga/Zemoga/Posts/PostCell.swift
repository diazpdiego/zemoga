//
//  PostCell.swift
//  Zemoga
//
//  Created by Diego Diaz on 24/07/20.
//  Copyright © 2020 Diego Diaz. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {
    
    //MARK: - Config
    
    private enum StateImages {
        static let unReadImage = UIImage(named: "blue-dot-icon")
        static let favoriteImage = UIImage(named: "star-icon")
    }

    //MARK: - Properties
    
    @IBOutlet weak var stateImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Configure

    func configure(with postModel: PostModel) {
        let isFavorite  = postModel.isFavorite ?? false
        let isRead      = postModel.isRead ?? false
        
        stateImageView.image = isFavorite ? StateImages.favoriteImage : !isRead ? StateImages.unReadImage : nil
        titleLabel.text = postModel.title
        bodyLabel.text = postModel.body
    }
    
}
