//
//  PostModel.swift
//  Zemoga
//
//  Created by Diego Diaz on 24/07/20.
//  Copyright © 2020 Diego Diaz. All rights reserved.
//

import Foundation

struct PostModel: Decodable {
    var userId: Int?
    var id: Int?
    var title: String?
    var body: String?
    var isFavorite: Bool?
    var isRead: Bool?
}
