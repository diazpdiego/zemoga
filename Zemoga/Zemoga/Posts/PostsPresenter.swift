//
//  PostsPresenter.swift
//  Zemoga
//
//  Created by Diego Diaz on 24/07/20.
//  Copyright © 2020 Diego Diaz. All rights reserved.
//

import Foundation

struct PostsPresenter {
    
    //MARK: - Properties

    var repository = Repository()
    
    //MARK: - Public Methods

    func fetchPosts(completion: @escaping (_ posts: [PostModel]?) -> Void) {
        repository.getPost { (data) in
            guard let postData = data else {
                completion(nil)
                return
            }
            
            let results = self.parsePosts(postData)
            completion(results)
        }
        
    }

    func filterFavorites(on posts: [PostModel]) -> [PostModel] {
        return posts.filter{ $0.isFavorite ?? false }
    }
    
    func updateReadStateForPost(at index: Int, from posts: [PostModel]) -> [PostModel] {
        var postsArray = posts
        postsArray[index].isRead = true
        
        return postsArray
    }
    
    func updateFavoriteForPost(with postId: Int, from posts: [PostModel], isFavorite: Bool) -> [PostModel] {
        let index = posts.firstIndex{$0.id == postId}
        
        var newPosts = posts
        newPosts[index!].isFavorite = isFavorite
        
        return newPosts
    }
    
    func removePost(at index: Int, from posts: [PostModel]) -> [PostModel] {
        var postsArray = posts
        postsArray.remove(at: index)
        
        return postsArray
    }
    
    func removeFavorite(at index: Int, favorites: [PostModel], posts: [PostModel], completion: @escaping (_ favorites: [PostModel], _ posts: [PostModel]) -> Void) {
        let post = favorites[index]
        
        var newFavorites    = favorites
        var newPosts        = posts
        
        newFavorites.remove(at: index)
        newPosts.removeAll{ $0.id == post.id }
        
        completion(newFavorites, newPosts)
    }
    
    func removeAll(posts: [PostModel], favorites: [PostModel], completion: @escaping (_ post: [PostModel], _ favorites: [PostModel]) -> Void) {
        var emptyPostsArray = posts
        emptyPostsArray.removeAll()
        
        var emptyFavorites = favorites
        emptyFavorites.removeAll()
        
        completion(emptyPostsArray, emptyFavorites)
    }
    
    //MARK: - Private Methods

    private func parsePosts(_ data: Data) -> [PostModel]? {
        let decoder = JSONDecoder()
        
        guard let postsModel = try? decoder.decode([PostModel].self, from: data) else {
            return nil
        }
        
        return configureInitialPosts(with: postsModel)
    }
    
    private func configureInitialPosts(with postsArray: [PostModel]) -> [PostModel] {
        var posts = postsArray
        
        for index in 0...19 {
            posts[index].isRead = false
        }
        
        for index in 20...postsArray.count - 1 {
            posts[index].isRead = true
        }
        
        return posts
    }
}
