//
//  MasterViewController.swift
//  Zemoga
//
//  Created by Diego Diaz on 24/07/20.
//  Copyright © 2020 Diego Diaz. All rights reserved.
//

import UIKit
import CoreData

protocol PostSelectionDelegate: class {
  func postSelected(_ post: PostModel)
}

final class PostsViewController: UITableViewController {
    
    //MARK: - Config
    
    private enum Config {
        static let cellIdentifier           = "PostCell"
        static let defaultCellHeight        : CGFloat = 70
        static let defaultUnReadMessages    = 20
        static let removeAllButtonTitle     = "Remove All"
        static let emptyButtonTitle         = ""
        static let allPostsTitle            = "All"
        static let favoritesPostTitle       = "Favorites"
        static let showDetailSegue          = "showDetail"
    }
    
    private enum FilterOptionType {
        case All
        case Favorites
    }

    //MARK: - Properties
    
    var detailViewController: DetailViewController? = nil
    var managedObjectContext: NSManagedObjectContext? = nil
    weak var delegate: PostSelectionDelegate?

    private var presenter = PostsPresenter()
    private var filterOption = FilterOptionType.All
    private var postsArray: [PostModel] = []
    private var favorites: [PostModel] = []
    private let segmentedControlItems: [FilterOptionType] = [.All, .Favorites]

    @IBOutlet weak var removeAllButton: UIBarButtonItem!
    @IBOutlet weak var filterOptionsSegmented: UISegmentedControl!
    
    //MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        fetchPosts()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    //MARK: - Private Methods

    private func setupUI() {
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(fetchPosts))
        navigationItem.rightBarButtonItem = refreshButton
        
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        let nib = UINib(nibName: Config.cellIdentifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: Config.cellIdentifier)
        
        self.navigationController?.navigationBar.topItem?.title = Config.emptyButtonTitle
    }
        
    @objc
    private func fetchPosts() {
        presenter.fetchPosts { [weak self] (postsResults) in
            guard let posts = postsResults else { return }
            
            DispatchQueue.main.async {
                self?.postsArray = posts
                self?.updateRemoveAllButton()

                self?.tableView.reloadData()
            }
        }
    }

    private func updateRemoveAllButton() {
        self.removeAllButton.title = !self.postsArray.isEmpty ? Config.removeAllButtonTitle : Config.emptyButtonTitle
    }
    
    private func removePost(at index: Int) {
        if filterOption == .All {
            postsArray = presenter.removePost(at: index, from: postsArray)
            tableView.reloadData()
        } else {
            presenter.removeFavorite(at: index, favorites: favorites, posts: postsArray) { [weak self] (newFavorites, newPosts) in
                self?.favorites = newFavorites
                self?.postsArray = newPosts
                
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    private func updateStatePost(for index: Int) {
        postsArray = presenter.updateReadStateForPost(at: index, from: postsArray)
        tableView.reloadData()
    }

    //MARK: - Actions
    
    @IBAction func removeAllPosts(_ sender: Any) {
        presenter.removeAll(posts: postsArray, favorites: favorites) { [weak self] (newPosts, newFavorites) in
            self?.postsArray = newPosts
            self?.favorites = newFavorites
            
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.updateRemoveAllButton()
            }
        }
    }
    
    @IBAction func filterPosts(_ sender: UISegmentedControl) {
        let selectedOption = segmentedControlItems[sender.selectedSegmentIndex]
        switch selectedOption {
        case .All:
            filterOption = .All
        case .Favorites:
            filterOption = .Favorites
            favorites = presenter.filterFavorites(on: postsArray)
        }
        
        tableView.reloadData()
        updateRemoveAllButton()
    }

}

// MARK: - Table View

extension PostsViewController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Config.defaultCellHeight
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterOption == FilterOptionType.All ? postsArray.count : favorites.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PostCell = tableView.dequeueReusableCell(withIdentifier: Config.cellIdentifier) as! PostCell
        let posts = filterOption == .All ? postsArray : favorites
        cell.configure(with: posts[indexPath.row])
        
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            removePost(at: indexPath.row)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let posts = filterOption == .All ? postsArray : favorites
        delegate?.postSelected(posts[indexPath.row])

        if
            let detailViewController = delegate as? DetailViewController,
            let detailNavigationController = detailViewController.navigationController {
            splitViewController?.showDetailViewController(detailNavigationController, sender: nil)
        }
        
        updateStatePost(for: indexPath.row)
    }
}

//MARK: - Favorite Delegate

extension PostsViewController: FavoriteDelegate {
    func addToFavorites(_ postId: Int) {
        postsArray = presenter.updateFavoriteForPost(with: postId, from: postsArray, isFavorite: true)
        tableView.reloadData()
    }
    
    func removeFavorite(_ postId: Int) {
        postsArray = presenter.updateFavoriteForPost(with: postId, from: postsArray, isFavorite: false)
        tableView.reloadData()
    }
}

