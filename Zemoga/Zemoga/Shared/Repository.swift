//
//  Repository.swift
//  Zemoga
//
//  Created by Diego Diaz on 24/07/20.
//  Copyright © 2020 Diego Diaz. All rights reserved.
//

import Foundation

typealias requestCompletionHandler = (_ data: Data?) -> ()

struct Repository {
    
    //MARK: - Configs
    
    private enum EndPoints {
        static let baseUrl          = "https://jsonplaceholder.typicode.com/"
        static let postsPath        = "posts"
        static let commentsPath     = "posts/%i/comments"
        static let userInfoPath     = "users/%i"
    }
    
    private enum RequestType {
        static let httpGetMethodKey    = "GET"
        static let applicationJsonKey  = "application/json"
        static let contentTypeKey      = "Content-Type"
    }
    
    //MARK: - Public Methods
    
    func getPost(completion: @escaping (_ dataResult: Data?) -> Void) {
        guard let url = URL(string: EndPoints.baseUrl + EndPoints.postsPath) else {
            completion(nil)
            return
        }
        
        let request = makeUrlRequestWithUrl(url: url)
        sendRequest(with: request) { (data) in
            guard let dataResponse = data else {
                completion(nil)
                return
            }
            
            completion(dataResponse)
        }
    }
    
    func getUserInfo(to userId: Int, completion: @escaping (_ dataResult: Data?) -> Void) {
        let pathComplete = String(format: EndPoints.userInfoPath, userId)
        guard let url = URL(string: EndPoints.baseUrl + pathComplete) else {
            completion(nil)
            return
        }
        
        let request = makeUrlRequestWithUrl(url: url)
        sendRequest(with: request) { (data) in
            guard let dataResponse = data else {
                completion(nil)
                return
            }
            
            completion(dataResponse)
        }
    }
    
    func getComments(to postId: Int, completion: @escaping (_ dataResult: Data?) -> Void) {
        let pathComplete = String(format: EndPoints.commentsPath, postId)
        guard let url = URL(string: EndPoints.baseUrl + pathComplete) else {
            completion(nil)
            return
        }
        
        let request = makeUrlRequestWithUrl(url: url)
        sendRequest(with: request) { (data) in
            guard let dataResponse = data else {
                completion(nil)
                return
            }
            
            completion(dataResponse)
        }
    }
    
    //MARK: - Private Methods
    
    private func makeUrlRequestWithUrl(url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = RequestType.httpGetMethodKey
        request.setValue(RequestType.applicationJsonKey, forHTTPHeaderField: RequestType.contentTypeKey)
        
        return request
    }
    
    private func sendRequest(with request: URLRequest, withCompletion completion: @escaping requestCompletionHandler) {
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let dataResponse = data else {
                completion(nil)
                return
            }
            completion(dataResponse)
        })
        task.resume()
    }
}
